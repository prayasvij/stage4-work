package com.cognizant.DAO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor

public class Account {
	private String number;
	private String type;
	private String balance;
	
	public Account(String number, String type, String balance) {
		super();
		this.number = number;
		this.type = type;
		this.balance = balance;
	}
}
