package com.cognizant.springlearnemployee.Controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.springlearnemployee.EmployeeService;
import com.cognizant.springlearnemployee.Employee.DepartmentService;
import com.cognizant.springlearnemployee.Employee.Employee;

@RestController
public class DepartmentController {
	
	@GetMapping("/department")
	public List getAllDepartment(){
		System.out.println("employee controller start");
		return new DepartmentService().getAllDepartments();
		
	}

}
