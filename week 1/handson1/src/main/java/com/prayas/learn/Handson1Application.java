package com.prayas.learn;

import java.awt.DisplayMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ThreadPoolExecutor.DiscardOldestPolicy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class Handson1Application {
	private static final Logger LOGGER = LoggerFactory.getLogger(Handson1Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Handson1Application.class, args);

//		displayDate();
//		displayCountry();
		displayCounrties();
		System.out.println("end of code");
	}

	public static void displayDate() {
		LOGGER.info("START");
		ApplicationContext context = new ClassPathXmlApplicationContext("date-format.xml");
		SimpleDateFormat format = context.getBean("dateFormat", SimpleDateFormat.class);
		String date = "31/12/2018";
//	System.out.println(format.parse("31/12/2018"));
		LOGGER.debug("hello world");

		LOGGER.info("END");
	}

	public static void displayCountry() {
		ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
		Country country = (Country) context.getBean("country", Country.class);
		Country anotherCountry = context.getBean("country", Country.class);
		LOGGER.info("Start");
		LOGGER.debug("Country : {}", country.toString());
		LOGGER.info("end");
	}

	public static void displayCounrties() {
		ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
//		Country country = (Country) context.getBean("country", Country.class);
		ArrayList<Country> a = context.getBean("countryList", ArrayList.class);
		LOGGER.info("Start");
		LOGGER.debug("Country : {}", a.toString());
		LOGGER.info("end");
	}
//	public static void displayDate() throws ParseException {
//		ApplicationContext context = new ClassPathXmlApplicationContext("date-format.xml");
//		SimpleDateFormat format = context.getBean("dateFormat", SimpleDateFormat.class);
//		System.out.println(format.parse("31/12/2018"));
//	}

}
