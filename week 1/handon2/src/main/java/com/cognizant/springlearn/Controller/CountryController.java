package com.cognizant.springlearn.Controller;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.springlearn.dao.Country;
import com.cognizant.springlearn.exception.CountryNotFoundException;
import com.cognizant.springlearn.service.CountryService;

@RestController
public class CountryController {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private CountryService countryService;

	@GetMapping("/country")
	public Country getCountryIndia() {
		ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
		Country country = (Country) context.getBean("in", Country.class);

		log.info("Start");
		log.debug("Country : {}", country.toString());
		log.info("end");
		return country;
	}

	@GetMapping("/countries")
	public ArrayList<Country> getAllCountries() {
		ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
		ArrayList<Country> a = context.getBean("countryList", ArrayList.class);
		log.info("Start");
		log.debug("Country : {}", a.toString());
		log.info("end");
		return a;

	}

	@GetMapping("/country/{code}")
	public Country getCountry(@PathVariable String code) throws CountryNotFoundException {
		log.debug("code is", code);
		return countryService.getCountry(code);
	}
}
