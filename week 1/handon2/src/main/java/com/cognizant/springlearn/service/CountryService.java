package com.cognizant.springlearn.service;

import java.util.ArrayList;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.cognizant.springlearn.dao.Country;
import com.cognizant.springlearn.exception.CountryNotFoundException;

@Service
public class CountryService {

	public Country getCountry(String code) throws CountryNotFoundException {
		ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
		ArrayList<Country> a = context.getBean("countryList", ArrayList.class);
		Country country = null;
		for (Country c : a) {
			if (c.getCode().equalsIgnoreCase(code)) {
				country = c;
			}
		}
		if (country == null) {
			throw new CountryNotFoundException();
		}
		return country;
	}

}
