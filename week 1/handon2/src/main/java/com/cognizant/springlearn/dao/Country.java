package com.cognizant.springlearn.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Country {
	private String code;
	private String name;
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	public Country() {
		super();
		log.info("Inside Country Constructor");
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Country [code=" + code + ", name=" + name + "]";
	
	}

	public Country(String code, String name) {
		super();
		this.code = code;
		this.name = name;
		log.info("Inside Country Constructor");
	}

}
