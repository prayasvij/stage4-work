package com.cognizant.springlearn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Handon2Application {

	public static void main(String[] args) {
		SpringApplication.run(Handon2Application.class, args);
	}

}
