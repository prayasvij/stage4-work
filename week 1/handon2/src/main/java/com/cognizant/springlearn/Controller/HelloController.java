																																																																						package com.cognizant.springlearn.Controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@GetMapping("/hello")
	public String sayHello() {
		log.info("Start");
		log.debug("returning HELLO WORLD!!");
		log.info("end");
		return "Hello World!!";
	}

}
