package com.example.Moviecruiser.AdminController;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.Moviecruiser.Movie;

//import com.example.Moviecruiser.Admin.Movie;

@RestController
public class MovieController {
	
	@GetMapping("/movie")
	public Movie  movie() {
		System.out.println("hi movie");
		ApplicationContext context = new ClassPathXmlApplicationContext("Movie.xml");
    	Movie movie = (Movie)context.getBean("movie", Movie.class);

		return movie;
	}
	@PutMapping("/movieedit")
	public Movie editadmin(@RequestBody Movie movies ) {
		ApplicationContext context = new ClassPathXmlApplicationContext("Movie.xml");
    	Movie movie = (Movie)context.getBean("movie", Movie.class);
       if(!movie.getTitle().equals(movies.getTitle())){
    	   movie.setTitle(movies.getTitle());
       }else if(!movie.getBoxoffice().equals(movies.getBoxoffice())){
    	   movie.setBoxoffice(movies.getBoxoffice());
       }

		return movie;
	}

}
