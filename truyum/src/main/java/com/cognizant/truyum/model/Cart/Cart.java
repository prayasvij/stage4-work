package com.cognizant.truyum.model.Cart;

import java.util.ArrayList;
import java.util.List;

import com.cognizant.truyum.model.MenuItem.menuitem;

public class Cart {
	private String id;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public ArrayList<Integer> getCartItems() {
		return cartItems;
	}
	public void setCartItems(ArrayList<Integer> cartItems) {
		this.cartItems = cartItems;
	}
	private ArrayList<Integer> cartItems;	
}
