package com.cognizant.truyum.model.MenuItem;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table
public class menuitem {
	@Id
	 int id;
	 String name;
	 public menuitem(int id, String name, float price, boolean active,String category,
			boolean freedelivery) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.active = active;
		//this.dateOfLaunch = dateOfLaunch;
		this.category = category;
		this.freedelivery = freedelivery;
	}
	public menuitem() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
//	public String getDateOfLaunch() {
//		return dateOfLaunch;
//	}
//	public void setDateOfLaunch(String dateOfLaunch) {
//		this.dateOfLaunch = dateOfLaunch;
//	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public boolean isFreeDelivery() {
		return freedelivery;
	}
	public void setFreeDelivery(boolean freedelivery) {
		this.freedelivery = freedelivery;
	}
	float price;
	 boolean active;
	//String dateOfLaunch;
	 String category;
	boolean freedelivery;
	
}
