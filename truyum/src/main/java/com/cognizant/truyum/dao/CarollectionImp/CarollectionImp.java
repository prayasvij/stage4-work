package com.cognizant.truyum.dao.CarollectionImp;

import java.util.ArrayList;
import java.util.HashMap;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.cognizant.truyum.dao.CartDao.CartDao;
import com.cognizant.truyum.model.Cart.Cart;
import com.cognizant.truyum.model.MenuItem.menuitem;

import ch.qos.logback.classic.Logger;

@Repository
public class CarollectionImp implements CartDao {
	
	private static HashMap<String, Cart> cartItems = new HashMap<String, Cart>();

	private static ApplicationContext context = new ClassPathXmlApplicationContext("truyum.xml");
	private static ArrayList<menuitem> ITEM_LIST = context.getBean("menuItem", ArrayList.class);



	public void addCartItem(String userId, int menuItemId) {
		
		if (cartItems.isEmpty()) {
			Cart cart = new Cart();
			ArrayList<Integer> list = new ArrayList<Integer>();
			list.add(menuItemId);
			cart.setCartItems(list);
			cartItems.put(userId, cart);
			
		} else if (cartItems.containsKey(userId)) {
			Cart cart = cartItems.get(userId);
			ArrayList<Integer> list = cart.getCartItems();
						if (!list.contains(menuItemId))
				list.add(menuItemId);
			cart.setCartItems(list);
			cartItems.put(userId, cart);
			
		} else {
			Cart cart = new Cart();
			ArrayList<Integer> list = new ArrayList<Integer>();
					list.add(menuItemId);
			cart.setCartItems(list);
			cartItems.put(userId, cart);
			
		}

		
	}   
	
	
	
	public ArrayList<menuitem> getAllCartItems(String userId) {

		if (cartItems.containsKey(userId)) {
			Cart cart = cartItems.get(userId);
			ArrayList<Integer> list = cart.getCartItems();
			ArrayList<menuitem> itemlist = new ArrayList<menuitem>();

			for (int i = 0; i < list.size(); i++) {

				for (int j = 0; j < ITEM_LIST.size(); j++) {
					if (ITEM_LIST.get(j).getId() == list.get(i)) {
						itemlist.add(ITEM_LIST.get(j));
					}
				}
			}

			return itemlist;
		}
		return null;
	}


	public void deleteCartItem(String userId, int menuItemId) {
		
		if (cartItems.containsKey(userId)) {
			Cart cart = cartItems.get(userId);
			ArrayList<Integer> list = cart.getCartItems();
						if (list.contains(menuItemId)) {
				int index = list.indexOf(menuItemId);
				list.remove(index);
			}
			cart.setCartItems(list);
			cartItems.put(userId, cart);
			
		}
	
	}

}
	
	
