package com.cognizant.truyum.dao.MenuItemDao;


import java.util.List;

import com.cognizant.truyum.model.MenuItem.menuitem;

public interface MenuItemDao {
	 List<menuitem> getMenuItemListCustomer();
	 menuitem getMenuItem(int menuItemId); 
	 void modifyMenuItem(menuitem menu);
}
