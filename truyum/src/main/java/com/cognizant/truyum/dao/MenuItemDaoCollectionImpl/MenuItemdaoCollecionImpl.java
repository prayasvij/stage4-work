package com.cognizant.truyum.dao.MenuItemDaoCollectionImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.cognizant.truyum.MenuItemRepository;
import com.cognizant.truyum.dao.MenuItemDao.MenuItemDao;
import com.cognizant.truyum.model.MenuItem.menuitem;

@Service
public class MenuItemdaoCollecionImpl implements MenuItemDao{
	@Autowired
	MenuItemRepository menu;
//
	public List<menuitem> getMenuItemListCustomer(){
		   return (List<menuitem>) menu.findAll();
	
	}

public menuitem getMenuItem(int menuItemId) {
	
	Optional<menuitem> menukvj = menu.findById(menuItemId);
	
  return menukvj.get();
	
}

public void modifyMenuItem(menuitem menuitem) {
		menu.save(menuitem);
	
	
}	

}
