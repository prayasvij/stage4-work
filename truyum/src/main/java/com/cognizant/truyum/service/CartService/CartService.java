package com.cognizant.truyum.service.CartService;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.truyum.dao.CartDao.CartDao;
import com.cognizant.truyum.model.MenuItem.menuitem;

@Service
public class CartService {
	
	@Autowired
	CartDao cartDao;
	
		
	public void addCartItem(String userId, int menuItemId) {
		
		cartDao.addCartItem(userId, menuItemId);
		
	}
	
	
	
	public ArrayList<menuitem> getAllCartItems(String userId) {
		
		return cartDao.getAllCartItems(userId);		
	}

	public void deleteCartItem(String userId, int menuItemId) {
		
		cartDao.deleteCartItem(userId, menuItemId);
			}


}
