package com.cognizant.truyum.controller.MenuItemController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.truyum.MenuItemRepository;
import com.cognizant.truyum.model.MenuItem.menuitem;
import com.cognizant.truyum.service.MenuItemService.MenuItemService;

@RestController
@RequestMapping("/menu-items")
public class MenuItemController {
	@Autowired
	MenuItemService menuservice;
//	@Autowired
//	MenuItemRepository menu;
	
	@GetMapping("/getAllItem")
	public List<menuitem> getAllMenuItems(){
		System.out.println("controller");
		//return menuservice.getMenuItemListCustomer();
		return menuservice.getMenuItemListCustomer();
		
	}
	@GetMapping("/getAllItem/{id}")
	menuitem getMenuItem(@PathVariable int id) {
		return menuservice.getMenuItem(id);
		
	}
	@PutMapping("/saveitem")
	public void modifyMenuItem(@RequestBody menuitem menuit) {
		menuservice.modifyMenuItem(menuit);
		
	}


	

}
